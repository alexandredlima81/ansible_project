# MODELO DE README PARA A RAIZ

# Documentação e Scripts

Este repositório contém documentação e scripts relacionados à infraestrutura, divididos em diferentes ambientes: **On Premise**, **Colocation** e **AWS**. As instruções e scripts aqui contidos são estritamente para uso interno e estão sujeitos a aprovação da equipe responsável.


## Estrutura do Repositório


### On Premise

1. **Proxmox 7.2 (LEGADO)**
  - **/scripts**: Scripts relacionados ao Proxmox 7.2.
  - **/documentacao**: Documentação relacionada ao Proxmox 7.2.

2. **Proxmox 8.2**
  - **/scripts**: Scripts relacionados ao Proxmox 8.2.
  - **/documentacao**: Documentação relacionada ao Proxmox 8.2.
  - **/imagens**: Imagens relacionadas à documentação do Proxmox 8.2.
    - Exemplo: ![Arquitetura Proxmox](imagens/arquitetura-proxmox.jpg)