# ansible_project

### Overview

Ao estruturar seu projeto Ansible, é importante manter uma arquitetura de diretório clara e organizada. Embora não haja uma regra estrita sobre como estruturar seu projeto, aqui está uma abordagem comumente usada que pode ajudá-lo a manter um projeto Ansible limpo e escalável:

Inventory: Crie um diretório para armazenar seus arquivos de inventário. Esses arquivos definem os hosts e grupos que o Ansible irá gerenciar. Você pode organizá-los ainda mais com base em diferentes ambientes ou funções.

Playbooks:  crie um diretório especificamente para seus playbooks. Playbooks são arquivos YAML que definem as tarefas e configurações a serem aplicadas aos seus hosts. Organize seus playbooks com base em sua finalidade ou funcionalidade. Por exemplo, você pode ter diretórios separados para playbooks no nível do sistema, playbooks específicos do aplicativo ou playbooks para ambientes específicos.

Roles:  as funções do Ansible permitem encapsular tarefas, modelos e variáveis ​​reutilizáveis. Crie um diretório chamado "roles" para armazenar suas definições de função. Dentro desse diretório, organize suas funções com base em sua finalidade ou funcionalidade. Cada função deve ter seu próprio diretório contendo tarefas, modelos, variáveis ​​e quaisquer outros arquivos necessários.

Group Variables: crie um diretório chamado "group_vars" para armazenar arquivos de variáveis ​​específicas de grupo. Variáveis ​​de grupo permitem definir variáveis ​​que se aplicam a grupos específicos de hosts. Você pode organizar ainda mais esses arquivos com base na estrutura do seu grupo de inventário.

Host Variables: crie um diretório chamado "host_vars" para armazenar arquivos de variáveis ​​específicas do host. As variáveis ​​de host permitem que você defina variáveis ​​que se aplicam a hosts individuais. Você pode organizar esses arquivos com base no nome do host ou em qualquer outro critério que faça sentido para o seu projeto.

Files and Templates: crie um diretório chamado "arquivos" para armazenar arquivos estáticos que precisam ser transferidos para os hosts. Se você usar modelos Jinja2, crie um diretório separado chamado "modelos" para armazenar esses arquivos.

Configuration: crie um diretório para armazenar seus arquivos de configuração do Ansible, como ansible.cfg. Esse diretório também pode conter quaisquer outros arquivos de configuração relacionados ao seu projeto, como configurações específicas de inventário.

Documentation: é sempre uma boa prática ter um diretório de documentação que inclua qualquer documentação relevante ou arquivos README explicando o propósito e uso do seu projeto

## About structure folders
my_ansible_project/
├── inventory/
│   ├── production/
│   │   ├── hosts.yml
│   │   └── group_vars/
│   │       └── all.yml
│   ├── staging/
│   │   ├── hosts.yml
│   │   └── group_vars/
│   │       └── all.yml
│   └── development/
│       ├── hosts.yml
│       └── group_vars/
│           └── all.yml
├── playbooks/
│   ├── system/
│   │   ├── upgrade.yml
│   │   └── security.yml
│   ├── applications/
│   │   ├── webserver.yml
│   │   └── database.yml
│   └── environment/
│       ├── staging.yml
│       └── production.yml
├── roles/
│   ├── common/
│   │   ├── tasks/
│   │   │   └── main.yml
│   │   ├── templates/
│   │   │   └── config.j2
│   │   └── vars/
│   │       └── main.yml
│   ├── webserver/
│   │   ├── tasks/
│   │   │   └── main.yml
│   │   ├── templates/
│   │   │   └── nginx.conf.j2
│   │   └── vars/
│   │       └── main.yml
│   └── database/
│       ├── tasks/
│       │   └── main.yml
│       ├── templates/
│       │   └── my.cnf.j2
│       └── vars/
│           └── main.yml
├── group_vars/
│   ├── production.yml
│   ├── staging.yml
│   └── development.yml
├── host_vars/
│   ├── server1.yml
│   ├── server2.yml
│   └── server3.yml
├── files/
│   ├── app.tar.gz
│   └── config.ini
├── templates/
│   └── nginx.conf.j2
├── ansible.cfg
└── README.md

Nesta estrutura de exemplo:

Inventory: diretório contém subdiretórios para diferentes ambientes ( production, staging, development). Cada diretório de ambiente contém um hosts.ymlarquivo que define os hosts desse ambiente, junto com um group_varsdiretório para variáveis ​​específicas do grupo.

Playbooks: diretório é dividido em subdiretórios com base na funcionalidade ( system, applications, environment). Cada subdiretório contém arquivos de playbook YAML.

Roles: diretório contém subdiretórios para funções diferentes ( common, webserver, database). Cada diretório de função possui um tasksdiretório para tarefas, um templatesdiretório para modelos e um varsdiretório para variáveis.

Group Variables: diretório contém arquivos de variáveis ​​específicos do grupo ( production.yml, staging.yml, development.yml).

Host Variables: diretório contém arquivos de variáveis ​​específicos do host ( server1.yml, server2.yml, server3.yml).

Files: diretório armazena arquivos estáticos que precisam ser transferidos para os hosts.

Templates: diretório armazena modelos Jinja2.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/alexandredlima81/ansible_project.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/alexandredlima81/ansible_project/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.



O projeto também inclui um ansible.cfgarquivo para configuração do Ansible e um README.mdarquivo para documentação.